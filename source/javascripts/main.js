$(document).ready(function() {

	// scroll to top
	$('#scroll-top').click(function () {
		$('body, html').animate({
			scrollTop: 0
		}, 500);
	});

	// rating circle
	$('.rate-radial').each(function() {
		var progressCircle = $(this).find('.rate-radial__over'),
			fromOffset = progressCircle.attr('stroke-dasharray'),
			to = progressCircle.attr('data-to'),
			toOffset = (100 - to) * fromOffset / 100;
		progressCircle.attr('stroke-dashoffset', toOffset);
	});

	// show/hide site on list
	$('.site-list__item-bb').click(function (){
		$(this).parent('.site-list__item').toggleClass('site-list__item--hidden');
	});

	var sliders = document.querySelectorAll('.range');
	[].forEach.call(sliders, function(slider) {
		noUiSlider.create(slider, {
			start: [ 0 ],
			connect: 'lower',
			step: 1,
			range: {
				'min': [0],
				'max': [5]
			},
			pips: {
				mode: 'values',
		values: [0, 1, 2, 3, 4, 5],
		density: 100
			}
		});
	});

	// scroll to anchor
	$('.scroll-to').click(function (e){
		e.preventDefault();
		var href = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(href).offset().top-40
		}, 800);
	});

	// tabs
	$('.c-buttons__item').click(function (e){
		e.preventDefault();
		var tab_id = $(this).attr('href');
		$('.c-buttons__item').removeClass('c-buttons__item--active');
		$('.tab').removeClass('tab--active');
		$(this).addClass('c-buttons__item--active');
		$(tab_id).addClass('tab--active');
	});

	$('.c-stories').isotope({
		itemSelector: '.c-stories__item',
		percentPosition: true,
		masonry: {
			columnWidth: '.c-stories__sizer',
			gutter: '.c-stories__gutter'
		}
	});

});